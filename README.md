# FastAPI script
### a shell script to simplify creating fastapi projects

## How to use
1. clone the repo to your local machine
2. add the path of fastapi.sh file to the end of ~/.bashrc or ~/.zshrc as follows: ```source path/to/fastapi.sh```
3. run ```source ~/.bashrc``` or ```source ~/.zshrc``` whichever one you use
4. enjoy the commands

## Commands:

- ### fastapi init:
takes 2 arguments: project name and path
- initiate project with the given name
- create virtual environment
- install dependencies
- initiate alembic for migrations
- create initial directories and config files for project, database, test and authentication
#### examples: 
```bash
fastapi init my_project path/to/directory
```
***
- ### fastapi make:
- app: create a subproject with router, handler, models, schemas and tests \
takes argument the name of the app
- migrations: auto generate alembic revision with given message
#### examples:
```bash
fastapi make app my_app
fastapi make migrations "initial migrations"
```
***
- ### fastapi migrate:
optionally takes 1 argument 'all' \
apply one migration if ran without 'all' \
apply all migrations (to the head) if ran with 'all'
#### examples:
```bash
fastapi migrate
fastapi migrate all
```
***
- ### fastapi rollback:
optionally takes 1 argument 'all' \
rollback one migration if ran without 'all' \
rollback all migrations (to the base) if ran with 'all'
#### examples:
```bash
fastapi rollback
fastapi rollback all
``` 
***
- ### fastapi test:
run all the tests written in /tests directory
#### examples:
```bash
fastapi test
```
***
- ### fastapi run:
start the application on localhost and port 8000
#### examples:
```bash
fastapi run
```
