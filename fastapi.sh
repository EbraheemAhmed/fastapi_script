#!/bin/bash

#       function to automate and facilitate creating FastAPI project
fastapi(){


	#   if initiating a project
	if [ $1 = "init" ]
	then

		#   create base dir
		if [[ -d "$3/$2" ]]
		then
			echo -e "Directory $3/$2 already exists .. skip making ..."
		else
			mkdir $3/$2
		fi
		cd $3/$2
	
		#   install dependencies
		pipenv install fastapi pydantic pydantic-settings pydantic[email] sqlalchemy sqlalchemy-utils alembic uvicorn gunicorn pytest pytest-asyncio passlib[bcrypt] python-jose[cryptography] httpx psycopg2-binary asyncpg aiosqlite
		
		#   initiate alembic
		pipenv run alembic init migrations

		#   create .env file
		if [[ -f ".env" ]]
		then
			echo -e ".env file already exists .. skip making ..."
		else
			echo -e "$default_env" > .env 
			echo -e ".env file created"
		fi
		#	create default git ignore file
		if [[ -f ".gitignore" ]]
		then
			echo -e ".gitignore file already exists .. skip making ..."
		else 
			echo -e "$default_ignore" > .gitignore 
			echo -e ".gitignore file created"
		fi
		
		#   create project config file
		if [[ -f "config.py" ]]
		then
			echo -e "config file already exists .. skip making ..."
		else
			echo -e "$default_conf" > config.py 
			echo -e "config file created"
		fi

		#	create database client and test database client inside database directory
		prepare_database_dir

		#   create main file
		if [[ -f "main.py" ]]
		then
			echo -e "main file already exists .. skip making ..."
		else
			echo -e "$default_main" > main.py
			echo -e "main file created"
		fi

		#   create generics dir
		if [[ -d "generics" ]]
		then
			echo -e "generics dir already exists .. skip making ..."
		else
			echo -e "$default_exceptions" > generics/exceptions.py
			echo -e "$default_permissions" > generics/permissions.py
			echo -e "$default_mixins" > generics/mixins.py

			echo  "generics dir created successfully"
		fi

		#	prepare auth app
		prepare_auth

		#	prepare tests config
		echo -e "$default_conftest" > conftest.py

		create_app users

		cd -
	fi
	
	# if creating something
	if [ $1 = "make" ]
	then
		#   if creating new app
		if [ $2 = "app" ]
		then
			create_app $3
		fi

		#	if creating migrations
		if [ $2 = "migrations" ]
		then
			pipenv run alembic revision --autogenerate -m "$3"
			echo "revision created, double check it for mistakes"
		fi

	fi

	#	if migrating
	if [ $1 = "migrate" ]
	then
		if [ $2 = "all" ]
		then
			pipenv run alembic upgrade head
		else
			pipenv run alembic upgrade +1
		fi
	fi

	#	if rolling back
	if [ $1 = "rollback" ]
	then
		if [ $2 = "all" ]
		then
			pipenv run alembic downgrade base
		else
			pipenv run alembic downgrade -1
		fi
	fi

	#	if testing
	if [ $1 = "test" ]
	then
		pipenv run pytest -v -s -x --disable-warnings .
	fi

	#   running server
	if [ $1 = "run" ]
	then
		pipenv run uvicorn main:app --reload
	fi
}

prepare_database_dir(){
	if [[ -d "database" ]]
	then
		echo -e "database dir already exists .. skip making ..."
	else
		mkdir database
		#   create sync database client
		echo -e "$default_db_client" > database/client.py
		#   create async database client
		echo -e "$default_async_db_client" > database/async_client.py
		#   create test database client
		echo -e "$default_test_db_client" > database/test_client.py
		#	create database init
		echo -e "$default_db_init" > database/__init__.py

		echo -e "database dir prepared"
	fi
}

prepare_auth(){
	if [[ -d "authentication" ]]
	then
		echo -e "authentication dir already exists .. skip making ..."
	else
		#	create module
		mkdir authentication
		touch authentication/__init__.py
		#	create utils file
		echo -e "$default_utils" > authentication/utils.py
		#	create oauth2 file
		echo -e "$default_oath2" > authentication/oauth2.py
		#	create auth schema
		echo -e "$default_token_payload" > schemas/authentication.py

		echo -e "authentication dir prepared"
	fi
}

create_app(){
	if [[ -d "$1" ]]
	then
		echo -e "app with given name already exists"
	else
		mkdir $1
		touch $1/__init__.py
		#	create router
		echo -e "$default_router" | sed -e "s/HANDLERNAME/${1^}/g" -e "s/ROUTERNAME/$1/g" > $1/router.py
		#	create models
		echo -e "${default_model/MODELNAME/${1^:0:-1}}" > $1/models.py
		#	create schemas
		echo -e "$default_schema" | sed -e "s/SCHEMANAME/${1^}/g" > $1/schemas.py
		#	create handler
		echo -e "${default_handler/HANDLERNAME/${1^}}" > $1/handler.py
		#	create tests
		echo -e "$default_test" > $1/test_$1.py

		echo "app created successfully"
	fi
}

default_env="
DATABASE_URL='sqlite:///db.sqlite3'
ASYNC_DATABASE_URL='sqlite+aiosqlite:///db.sqlite3'
TOKEN_URL='login'
SECRET_KEY='GO FUCK YOURSELF'
TOKEN_ENCODING_ALGORITHM='HS256'
ACCESS_TOKEN_EXPIRE_MINUTES=30
"

default_ignore="
Pipfile*
*cache*
.env
"

default_conf="
from pydantic_settings import BaseSettings
from importlib import import_module

class Settings(BaseSettings):
	#   secret key
	SECRET_KEY: str = 'insecure key please change it'

	#   jwt token settings
	TOKEN_URL: str = 'login'
	TOKEN_ENCODING_ALGORITHM: str = 'HS256'
	ACCESS_TOKEN_EXPIRE_MINUTES: int = 20

	#   database settings
	DATABASE_URL: str = 'sqlite+sqlite3:///db.sqlite3'
	ASYNC_DATABASE_URL: str = 'sqlite+aiosqlite:///db.sqlite3'

	#	installed apps
	APPS: list = []

	class Config:
		env_file = '.env'

settings = Settings()


MODELS = [
	getattr(import_module(f'{app}.models'), 'Base', None)
	for app in settings.APPS 
	if getattr(import_module(app), 'models', None) != None 
	and getattr(import_module(f'{app}.models'), 'Base', None) != None
]
"

default_db_init="
from .client import engine, get_db
from .async_client import async_engine, get_async_db
from .test_client import test_engine, get_test_db

from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
"

default_db_client="
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from config import settings



engine = create_engine(
	settings.DATABASE_URL,
	#connect_args={'check_same_thread': False}, # uncomment if using sqlite
	future=True # for using the new async orm
)

SessionLocal = sessionmaker(
	autocommit=False,
	autoflush=False,
	bind=engine,
	future=True
)

def get_db():
	db = SessionLocal()
	try:
		yield db
	finally:
		db.close()
"

default_async_db_client="
from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from sqlalchemy.orm import sessionmaker
from config import settings



async_engine = create_async_engine(
	settings.ASYNC_DATABASE_URL,
	#connect_args={'check_same_thread': False}, # uncomment if using sqlite
	future=True # for using the new async orm
)

AsyncSessionLocal = sessionmaker(
	async_engine,
	autocommit=False,
	autoflush=False,
	class_=AsyncSession,
	expire_on_commit=False,
)

async def get_async_db():
	async with AsyncSessionLocal() as db:
		try:
			yield db
		except Exception as e:
			await db.rollback()
			raise e
		finally:
			await db.close()
"

default_test_db_client="
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession


test_engine = create_engine(
	'sqlite:///test_db.sqlite3',
	#connect_args={'check_same_thread': False}, # uncomment if using sqlite
)

async_engine = create_async_engine(
	'sqlite+aiosqlite:///test_db.sqlite3',
	#connect_args={'check_same_thread': False}, # uncomment if using sqlite
)

AsyncSessionLocal = sessionmaker(
	async_engine,
	autocommit=False,
	autoflush=False,
	class_=AsyncSession,
	expire_on_commit=False,
)

async def get_test_db():
	async with AsyncSessionLocal() as db:
		try:
			yield db
		except Exception as e:
			await db.rollback()
			raise e
		finally:
			await db.close()
"

default_conftest="
import pytest
from fastapi.testclient import TestClient


from main import app
from authentication.oauth2 import create_access_token
from config import MODELS
from database import test_engine, get_test_db, get_async_db


Base = MODELS[-1]

@pytest.fixture(scope='function')
def client():
	Base.metadata.drop_all(bind=test_engine)
	Base.metadata.create_all(bind=test_engine)

	app.dependency_overrides[get_async_db] = get_test_db
	with TestClient(app) as client:
		yield client

@pytest.fixture(scope='function')
def test_user(client):
	user_data = {
		'email': 'email@example.com',
		'first_name': 'eepy',
		'last_name': 'sleepy',
		'password': 'password',
		'is_admin': True
	}
	res = client.post(
		'http://localhost:8000/users',
		json=user_data
	)
	assert res.status_code == 201
	user = res.json()
	user['password'] = user_data['password']
	return user

@pytest.fixture
def token(test_user):
	return create_access_token({'user_id': test_user['id']})

@pytest.fixture
def authorized_client(client, token):
	client.headers = {
		**client.headers,
		'Authorization': f'Bearer {token}'
	}
	return client
"

default_main="
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

origins = [
	'http://localhost',
	'http://127.0.0.1'
]
app = FastAPI(
	title = '',
	description = '',
	version = '',
	terms_of_service = '',
	contact = {
		'user': '',
		'url': '',
		'email': '',
	},
	license_info = {
		'name': '',
		'url': '',
	}
)
app.add_middleware(
	CORSMiddleware,
	allow_origins=origins,
	allow_credentials=True,
	allow_methods=['*'],
	allow_headers=['*']
)

from config import settings
from importlib import import_module


for app_name in settings.APPS:
	module_path = f'{app_name}.router'  # Adjust path as needed
	module = import_module(module_path)
	router = getattr(module, f'{app_name}_router', None)
	if app_name in ['authentication', 'user']:
		app.include_router(router, prefix='/accounts', tags=['authentication'])
		continue
	if app_name == 'upload':
		app.include_router(router, prefix='/data', tags=['uploads'])
		continue
	app.include_router(router, prefix=f'/{app_name}s', tags=[f'{app_name}s'])
"

default_exceptions="
from typing import Any, Dict, Optional
from typing_extensions import Annotated, Doc
from fastapi import HTTPException, status



class ForbiddenException(HTTPException):

    def __init__(self, detail: Annotated[Optional[str], 'User have no access rights']) -> None:
        super().__init__(status.HTTP_403_FORBIDDEN, detail)



class UnAuthorizedException(HTTPException):

    def __init__(self, detail: Annotated[Optional[str], 'UNAUTHORIZED']) -> None:
        super().__init__(status.HTTP_401_UNAUTHORIZED, detail, headers={'WWW-Authenticate': 'Bearer'})



class NotFoundException(HTTPException):

    def __init__(self, detail: Annotated[Optional[str], None]) -> None:
        super().__init__(status.HTTP_404_NOT_FOUND, detail)
"

default_utils="
from passlib.context import CryptContext


pwd_context = CryptContext(schemes=['bcrypt'], deprecated='auto')

def hash_password(password: str) -> str:
	return pwd_context.hash(password)

def verify_password(plain_password: str, hashed_password: str) -> bool:
	return pwd_context.verify(plain_password, hashed_password)
"

default_oath2="
from typing import Annotated
from fastapi import Depends, status, HTTPException
from fastapi.security import OAuth2PasswordBearer
from jose import JWTError, jwt
from datetime import datetime, timedelta
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select

from config import settings
from database import get_async_db
import models
import schemas

oauth2_scheme = OAuth2PasswordBearer(tokenUrl=settings.TOKEN_URL)

def create_access_token(payload: dict):
	data = payload.copy()
	expires = datetime.utcnow() + timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES)
	data.update({'exp': expires})

	token = jwt.encode(
		data,
		settings.SECRET_KEY,
		algorithm=settings.TOKEN_ENCODING_ALGORITHM,
	)
	return token

def verify_access_token(token: str, credentials_exceoption):
	try:
		payload = jwt.decode(
			token,
			settings.SECRET_KEY,
			algorithms=[settings.TOKEN_ENCODING_ALGORITHM]
		)
		id: str = payload.get('user_id')
		if not id:
			raise credentials_exceoption
	except JWTError:
		raise credentials_exceoption
	return schemas.TokenPayload(**payload)

async def get_current_user(token: Annotated[str, Depends(oauth2_scheme)], db: Annotated[AsyncSession, Depends(get_db)]):
	credentials_exceoption = HTTPException(
		status_code=status.HTTP_401_UNAUTHORIZED,
		detail='could not validate credentials',
		headers={'WWW-Authenticate': 'Bearer'}
	)
	token = verify_access_token(token, credentials_exceoption)
	user = await db.execute(
		select(models.User).where(models.User.id==token.user_id)
	)
	user = user.scalar_one()
	return user
"

default_permissions="
from typing import Any
from fastapi import Depends
from sqlalchemy import and_, or_, exists
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select

from authentication.oauth2 import get_current_user
from database import get_async_db
from generics.exceptions import ForbiddenException, UnAuthorizedException

from models.user import User, UserDivisions



class AdminPermission:

	def __init__(self, user: User = Depends(get_current_user)) -> None:
		if not user.is_admin:
			raise UnAuthorizedException()
		self.user = user
		


class Permission:

	def __init__(self, user: User = Depends(get_current_user), db: AsyncSession = Depends(get_async_db)) -> None:
		self.user = user
		self.db = db
		self.ForbiddenException = ForbiddenException()

	async def has_object_permission(self, id: Any) -> bool:
		pass


	async def check_permission(self, id: Any):
		if not (self.user.is_admin or await self.has_object_permission(id)):
			raise self.ForbiddenException
"

default_token_payload="
from pydantic import BaseModel

class TokenPayload(BaseModel):
	user_id: int
"

default_router="
from typing import Annotated, List
from fastapi import (
	APIRouter,
	Response,
	status,
	HTTPException,
	Depends,
	Path,
	Query
)

from sqlalchemy.ext.asyncio import AsyncSession

from database import get_async_db
from authentication.oauth2 import get_current_user
from users.models import User

from .schemas import HANDLERNAMECreate
from .handler import HANDLERNAMEHandler

ROUTERNAME_router = APIRouter()


@ROUTERNAME_router.get('', status_code=status.HTTP_200_OK)
async def get_all(
	user: Annotated[User, Depends(get_current_user)],
	db: Annotated[AsyncSession, Depends(get_async_db)]
):
	handler = HANDLERNAMEHandler(user, db)
	return await handler.get_all()


@ROUTERNAME_router.post('', status_code=status.HTTP_201_CREATED)
async def create(
	ROUTERNAME: HANDLERNAMECreate, 
	user: Annotated[User, Depends(get_current_user)],
	db: Annotated[AsyncSession, Depends(get_async_db)]
):
	handler = HANDLERNAMEHandler(user, db)
	return await handler.create(HANDLERNAMECreate)


@ROUTERNAME_router.get('/{id}', status_code=status.HTTP_200_OK)
async def get_one(
	id: int, 
	user: Annotated[User, Depends(get_current_user)],
	db: Annotated[AsyncSession, Depends(get_async_db)]
):
	handler = HANDLERNAMEHandler(user, db)
	return await handler.get_one(id)


@ROUTERNAME_router.put('/{id}', status_code=status.HTTP_200_OK)
async def update(
	id: int, 
	user: Annotated[User, Depends(get_current_user)],
	db: Annotated[AsyncSession, Depends(get_async_db)]
):
	handler = HANDLERNAMEHandler(user, db)
	return await handler.update(id, ROUTERNAME)


@ROUTERNAME_router.delete('/{id}', status_code=status.HTTP_204_NO_CONTENT)
async def delete(
	id: int, 
	user: Annotated[User, Depends(get_current_user)],
	db: Annotated[AsyncSession, Depends(get_async_db)]
):
	handler = HANDLERNAMEHandler(user, db)
	await handler.get(id)
	return await handler.delete(id)
"

default_schema="
from typing import Annotated, Any, Optional, List
from pydantic import BaseModel

class SCHEMANAMEBase(BaseModel):
	pass

class SCHEMANAMECreate(SCHEMANAMEBase):
	pass

class SCHEMANAME(SCHEMANAMEBase):
	id: Any
	class Config:
		from_attributes = True
"

default_model="
import uuid
import enum
from sqlalchemy import (
	Column,
	Integer,
	String,
	Boolean,
	ForeignKey,
	Enum,
	Text,
)
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship
from sqlalchemy.sql.sqltypes import TIMESTAMP
from sqlalchemy.sql.expression import text, null
from sqlalchemy_utils import EmailType, URLType

from database import Base


class MODELNAME(Base):
	__tablename__ = ''
	
	id = Column(
		Integer,
		primary_key=True,
		nullable=False
	) 
"

default_handler="
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select
from sqlalchemy import insert, update, delete
from sqlalchemy.orm import selectinload

from database import get_db, get_async_db
from exceptions import UserNotFoundException, ForbiddenException

from users.models import User


class HANDLERNAMEHandler:

	def __init__(self, user: User, db: AsyncSession):
		pass

	async def get_all(self):
		pass


	async def create(self):
		pass


	async def get(self):
		pass


	async def update(self):
		pass


	async def delete(self):
		pass
"

default_test="
import pytest

import schemas
"

default_mixins="
from datetime import datetime

from sqlalchemy import Column, DateTime
from sqlalchemy.orm import declarative_mixin

@declarative_mixin
class Timestamp:
	created_at = Column(Datetime, default=datetime.utcnow, nullable=False)
	updated_at = Column(Datetime, default=datetime.utcnow, nullable=False)
"